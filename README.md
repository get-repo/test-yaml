<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/53630391/testyaml_logo.png" height=100 />
</p>

<h1 align=center>TestYaml</h1>

<br/>

Write tests in YAML

## Table of Contents

1. [Installation](#installation)

## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/test-yaml git https://gitlab.com/get-repo/test-yaml.git
    composer require --dev get-repo/test-yaml
