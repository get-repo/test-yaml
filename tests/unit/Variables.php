<?php

namespace Test\unit;

class Variables
{
    public function __construct(
        private readonly string $value
    ) {
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function testParameters(Simple $simple, Mock $mock, string $string = null, array $array = null): string
    {
        return sprintf(
            'C=%s, S=%s, M=%s, S=%s%s',
            $this->value,
            $simple->methodWithMandatoryParam($this->value),
            $mock->mockSimple($simple),
            $string,
            $array ? ' A=' . count(array_filter(array_map('is_object', $array))) : '',
        );
    }
}
