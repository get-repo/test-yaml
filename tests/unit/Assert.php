<?php

namespace Test\unit;

class Assert
{
    public function getInteger(): int
    {
        return 54;
    }

    public function getString(): string
    {
        return 'abcdefgh';
    }

    public function getObject(): self
    {
        return $this;
    }

    public function getArray(): array
    {
        return [1, 2, 3, $this->getString(), $this, ['a', 'b']];
    }

    public function getNull(): null
    {
        return null;
    }

    public function getUnionType(string|object $param): string
    {
        return gettype($param);
    }

    public function exception(): void
    {
        throw new \ErrorException('message of the exception');
    }
}
