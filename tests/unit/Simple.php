<?php

namespace Test\unit;

class Simple
{
    public static function staticMethod(): string
    {
        return 'static value';
    }

    public static function staticMethodWithOptionalParam(string $optional = 'default'): string
    {
        return "static {$optional}";
    }

    public static function staticMethodWithMandatoryParam(?string $mandatory): ?string
    {
        return $mandatory;
    }

    public function method(): string
    {
        return 'value';
    }

    public function methodWithOptionalParam(string $optional = 'default'): string
    {
        return $optional;
    }

    public function methodWithMandatoryParam(mixed $mandatory): mixed
    {
        return $mandatory;
    }

    public function setNothing(): self
    {
        return $this;
    }
}
