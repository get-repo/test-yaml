<?php

namespace Test\unit;

use Symfony\Component\Console\Command\Command;

class Mock
{
    public function mock(Constructor $constructor): array
    {
        return $constructor->getValues();
    }

    public function mockSimple(Simple $simple): string
    {
        return $simple->method();
    }

    public function mockGreaterThan10(Constructor $constructor, int $int = 0): bool
    {
        return $constructor->greaterThan10($int);
    }

    public function getName(Command $command): string
    {
        return $command->getApplication()->getDefinition()->getSynopsis();
    }
}
