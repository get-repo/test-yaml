<?php

namespace Test\unit;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Instances
{
    public function countBag(ParameterBagInterface $bag): int
    {
        return count($bag->all());
    }

    public function getItem(string $name, ParameterBagInterface $bag): ?string
    {
        return $bag->has($name) ? $bag->get($name) : null;
    }
}
