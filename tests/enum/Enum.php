<?php

namespace Test\enum;

enum Enum: string
{
    case ONE = '1';
    case TWO = '2';
    case THREE = 'three';
    case FOUR = 'four';

    public function isString(int $max = 1): bool
    {
        return strlen($this->value) > $max;
    }
}
