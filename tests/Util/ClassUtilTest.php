<?php

namespace Test\Util;

use GetRepo\TestYaml\Configuration\EntityConfiguration;
use GetRepo\TestYaml\Util\ClassUtil;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;
use Test\unit\Constructor;
use Test\unit\Simple;

class ClassUtilTest extends TestCase
{
    public static function newInstanceProvider(): array
    {
        return [
            'empty parameter' => [
                [],
                Constructor::class . '::__construct() is missing required parameter "int".',
            ],
            'unnamed parameter' => [
                [66],
                Constructor::class . '::__construct() parameters must be always named.',
            ],
            'invalid parameter type' => [
                ['int' => ['array']],
                Constructor::class . '::__construct() parameter "int" is expecting type int, got array.',
            ],
            'valid parameters' => [['int' => 99]],
        ];
    }

    #[DataProvider('newInstanceProvider')]
    public function testNewInstance(array $params = [], string $exception = null): void
    {
        if ($exception) {
            $this->expectExceptionMessage($exception);
        }
        $simple = ClassUtil::newInstance(Constructor::class, $params);
        $this->assertInstanceOf(Constructor::class, $simple);
    }

    public static function validateMethodParametersProvider(): array
    {
        return [
            'not named parameters' => [
                'methodWithMandatoryParam',
                ['not named'],
                Simple::class . '::methodWithMandatoryParam() parameters must be always named.',
            ],
            'not enough parameters' => [
                'methodWithMandatoryParam',
                ['invalid' => true],
                Simple::class . '::methodWithMandatoryParam() is missing required parameter "mandatory".',
            ],
            'too much parameters' => [
                'methodWithMandatoryParam',
                ['mandatory' => 'ok', 'invalid' => true],
                Simple::class . '::methodWithMandatoryParam() contains invalid parameter(s): "invalid".',
            ],
            'valid parameters' => [
                'methodWithMandatoryParam',
                ['mandatory' => 'ok'],
            ],
        ];
    }

    #[DataProvider('validateMethodParametersProvider')]
    public function testValidateMethodParameters(string $method, array $params, string $exception = null): void
    {
        if ($exception) {
            $this->expectExceptionMessage($exception);
        }
        $rMethod = new ReflectionMethod(Simple::class, $method);
        $this->assertInstanceOf(ReflectionMethod::class, $rMethod); // makes phpunit happy
        ClassUtil::validateMethodParameters(new ReflectionMethod(Simple::class, $method), $params);
    }

    /**
     * phpcs:disable Generic.Files.LineLength.TooLong
     */
    public static function getMethodsProvider(): array
    {
        return [
            'all methods' => [
                ['getAssertNode', 'getConfigTreeBuilder', 'getDataNode', 'getInstancesNode', 'getMocksNode', 'getVariablesNode'],
            ],
            'option visibility public' => [
                ['getConfigTreeBuilder'],
                ['visibility' => \ReflectionMethod::IS_PUBLIC],
            ],
            'option visibility protected' => [
                ['getAssertNode', 'getDataNode', 'getInstancesNode', 'getMocksNode', 'getVariablesNode'],
                ['visibility' => \ReflectionMethod::IS_PROTECTED],
            ],
            'option visibility private' => [
                [],
                ['visibility' => \ReflectionMethod::IS_PRIVATE],
            ],
            'option regex' => [
                ['getAssertNode'],
                ['regex' => '/Assert/'],
            ],
            'option regex and visibility protected' => [
                ['getVariablesNode'],
                ['regex' => '/Variables/', 'visibility' => \ReflectionMethod::IS_PROTECTED],
            ],
        ];
    }

    #[DataProvider('getMethodsProvider')]
    public function testGetMethods(array $expected, array $options = []): void
    {
        $this->assertEquals(
            $expected,
            array_keys(ClassUtil::getMethods(new \ReflectionClass(EntityConfiguration::class), $options)),
        );
    }
}
