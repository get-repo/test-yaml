<?php

namespace Test\entity;

class Entity
{
    public function __construct(
        private readonly string $client,
        private readonly ?self $object = null,
    ) {
    }

    public function getClient(string $prefix = null): string
    {
        return sprintf('%s%s', $prefix ? "{$prefix}_" : '', $this->client);
    }

    public function getObject(): ?self
    {
        return $this->object;
    }

    public function exception(): void
    {
        throw new \ErrorException('message of the exception');
    }
}
