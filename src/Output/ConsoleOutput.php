<?php

namespace GetRepo\TestYaml\Output;

use Symfony\Component\Console\Output\OutputInterface;

class ConsoleOutput
{
    public const SIZE = 80;

    public function __construct(
        private OutputInterface $output,
    ) {
    }

    public function write(string|iterable $messages, bool $newline = false, int $options = 0): self
    {
        $this->output->write($messages, $newline, $options);

        return $this;
    }

    public function writeln(string|iterable $messages, int $options = 0): self
    {
        $this->output->writeln($messages, $options);

        return $this;
    }

    public function err(string $message, bool $newline = false): self
    {
        $this->colored('bright-red', $message, $newline);

        return $this;
    }

    public function warn(string $message, bool $newline = false): self
    {
        $this->colored('yellow', $message, $newline);

        return $this;
    }

    private function colored(string $style, string $message, bool $newline = false): void
    {
        $this->write(sprintf('<fg=%s>%s</>', $style, $message), $newline);
    }
}
