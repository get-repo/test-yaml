<?php

namespace GetRepo\TestYaml\Output;

use GetRepo\TestYaml\Loader\Test\AbstractTest;

class ErrorOutput
{
    public function __construct(
        private ConsoleOutput $output,
    ) {
    }

    /**
     * @param AbstractTest[] $tests
     */
    public function print(array $tests): void
    {
        $nbFailures = count($tests);
        $this->output->err(sprintf(
            '<fg=red;options=bold>%d</> failure%s:',
            $nbFailures,
            $nbFailures > 1 ? 's' : '',
        ), true);

        foreach ($this->group($tests) as $fileName => $fileTests) {
            $this->output->err(sprintf(
                '%s<fg=red;options=bold>%s</>:',
                str_repeat(' ', 4),
                basename($fileName),
            ), true);
            foreach ($fileTests as $testName => $itemTests) {
                $this->output->err(sprintf('%s%s:', str_repeat(' ', 8), $testName), true);
                foreach ($itemTests as $itemTestName => $asserts) {
                    foreach ($asserts as $assertName => $test) {
                        $exception = $test->getException();
                        $this->output->err(sprintf(
                            '%s%s→ %s: <fg=gray>%s</>',
                            str_repeat(' ', 12),
                            $itemTestName,
                            $assertName,
                            trim($exception->getMessage()),
                        ), true);
                    }
                }
            }
        }
    }

    /**
     * @param AbstractTest[] $tests
     */
    private function group(array $tests): array
    {
        $grouped = [];
        foreach ($tests as $test) {
            $grouped[$test->getFile()][$test->getTestName()][$test->getItemTestName()][$test->getAssertName()] = $test;
        }

        return $grouped;
    }
}
