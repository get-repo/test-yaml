<?php

namespace GetRepo\TestYaml\Util;

use Assert\Assertion;
use Closure;
use GetRepo\TestYaml\Configuration\AssertsConfiguration;
use GetRepo\TestYaml\Exception\TestFailedException;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\String\ByteString;
use Symfony\Component\Yaml\Yaml;

class AssertUtil
{
    public const CONFIG_PATH = 'config/asserts.yml';

    private static array $config;

    public static function getConfig(): array
    {
        if (!isset(self::$config)) {
            $config = (new Processor())->processConfiguration(
                new AssertsConfiguration(),
                [Yaml::parseFile(realpath(sprintf('%s/%s', dirname(dirname(__DIR__)), self::CONFIG_PATH)))],
            );

            // method
            foreach ($config as $k => &$v) {
                if (!$v['method']) {
                    $v['method'] = (new ByteString($k))->camel()->toString();
                }
            }
            unset($v);

            // property path and fill
            foreach ($config as $k => $v) {
                $v['property_path'] = false;
                self::$config[$k] = $v;
                $v['property_path'] = true;
                self::$config[sprintf('property_path_%s', $k)] = $v;
            }
        }

        return self::$config;
    }

    public static function run(mixed $actual, array $asserts = []): void
    {
        $pa = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();
        $configs = AssertUtil::getConfig();
        foreach ($asserts as $name => $expected) {
            $message = null;
            $config = $configs[$name];
            if ($config['property_path']) {
                $actual = $pa->getValue($actual, $expected['path']);
                $expected = $expected['value'];
            }
            if ($config['negate']) {
                if (!$expected) {
                    $config['method'] = sprintf('not%s', ucfirst($config['method']));
                }
                $expected = null; // message parameter
            }
            if (preg_match('/^is_(array|resource|traversable|countable|json|object|callable)$/', $name)) {
                Assertion::eq(true, $expected, sprintf(
                    'Using asserts like %1$s does not need an expected value (eg: "%1$s: ~")',
                    $name,
                ));
                $expected = null; // message parameter
            }
            if (preg_match('/^(property_path_)?array_keys$/', $name)) {
                Assertion::isArray($actual, sprintf(
                    'Actual value to assert array keys is not an array, got %s.',
                    gettype($actual),
                ));
                $config['method'] = 'eq';
                $actual = array_keys($actual);
                $message = '';
                if ($diff = array_diff($actual, $expected)) {
                    $message .= sprintf('Array key(s) missing: "%s". ', implode('", "', $diff));
                }
                if ($diff = array_diff($expected, $actual)) {
                    $message .= sprintf('Array key(s) not found: "%s". ', implode('", "', $diff));
                }
            }
            if (preg_match('/^(property_path_)?equals$/', $name) && is_array($actual) && is_array($expected)) {
                if ($diff = array_diff($actual, $expected)) {
                    $message .= sprintf('Array values(s) missing: "%s". ', implode('", "', $diff));
                }
                if ($diff = array_diff($expected, $actual)) {
                    $message .= sprintf('Array values(s) not found: "%s". ', implode('", "', $diff));
                }
            }

            $closure = Closure::fromCallable([Assertion::class, $config['method']]);
            $closure($actual, $expected, $message);
        }
    }

    public static function runAndCatchException(\Closure $closure, array $parameters, array $asserts = []): void
    {
        try {
            $actual = $closure(...$parameters);
        } catch (\Exception $e) {
            $actual = $e;
        }

        try {
            AssertUtil::run($actual, $asserts);
        } catch (\Error $e) {
            throw new TestFailedException(sprintf(
                '%s (in %s:%d)',
                $e->getMessage(),
                basename($e->getFile()),
                $e->getLine(),
            ), $e);
        }
    }
}
