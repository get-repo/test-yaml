<?php

namespace GetRepo\TestYaml\Util;

use ReflectionException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassUtil
{
    public static function newInstance(string $className, array $params = []): object
    {
        $rClass = new \ReflectionClass($className);
        if ($rClass->isAbstract()) {
            return eval(sprintf(
                'Can not instantiate abstract class %s',
                $className,
            ));
        }
        if ($constructor = $rClass->getConstructor()) {
            self::validateMethodParameters($constructor, $params);
        }

        return $rClass->newInstanceArgs($params);
    }

    public static function validateMethodParameters(\ReflectionMethod $rMethod, array $params): void
    {
        if ($params && [true] === array_unique(array_map('is_int', array_keys($params)))) {
            throw new ReflectionException(sprintf(
                '%s::%s() parameters must be always named.',
                $rMethod->getDeclaringClass()->getName(),
                $rMethod->getName(),
            ));
        }

        $invalid = $params;
        foreach ($rMethod->getParameters() as $parameter) {
            $name = $parameter->getName();
            $required = !$parameter->isOptional();
            $exists = array_key_exists($name, $params);

            if ($required && !$exists) {
                throw self::createReflectionException($rMethod, sprintf(
                    'is missing required parameter "%s"',
                    $name,
                ));
            }
            if ($parameter->hasType() && $exists) {
                $value = $params[$name];
                $expecting = is_object($value) ? get_class($value) : gettype($value);
                $expecting = match ($expecting) {
                    'integer' => 'int',
                    'boolean' => 'bool',
                    default => $expecting,
                };

                $matcher = function (string &$actual) use ($rMethod): void {
                    $actual = match ($actual) {
                        'self' => $rMethod->getDeclaringClass()->getName(),
                        default => $actual,
                    };
                };
                /** @var \ReflectionType $rType */
                $rType = $parameter->getType();
                // handle union type separately
                if ($rType instanceof \ReflectionUnionType) {
                    $expectings = [];
                    foreach ($rType->getTypes() as $rType) {
                        $actual = $rType->getName();
                        $matcher($actual);
                        $expectings[] = $actual;
                    }
                    $actual = implode('|', $expectings);
                    unset($expectings);
                    if (!str_contains($actual, $expecting)) {
                        throw self::createReflectionException($rMethod, sprintf(
                            'Union type parameter "%s" is expecting type %s, got %s',
                            $name,
                            $actual,
                            $expecting,
                        ));
                    }
                } else {
                    /** @var \ReflectionNamedType $rType */
                    $actual = $rType->getName();
                    $matcher($actual);
                    if (
                        'mixed' !== $actual
                        && (
                            (!is_object($value) && $expecting !== $actual)
                            ||
                            (is_object($value)) && $actual !== 'object' && !($value instanceof $actual)
                        )
                    ) {
                        throw self::createReflectionException($rMethod, sprintf(
                            'parameter "%s" is expecting type %s, got %s',
                            $name,
                            $actual,
                            $expecting,
                        ));
                    }
                }
            }
            unset($invalid[$name]);
        }

        if ($invalid) {
            throw self::createReflectionException($rMethod, sprintf(
                'contains invalid parameter(s): "%s"',
                implode('", "', array_keys($invalid)),
            ));
        }
    }

    public static function getMethods(\ReflectionClass $rClass, array $options = []): array
    {
        $resolver = new OptionsResolver();
        $resolver->setDefault('visibility', null);
        $resolver->setAllowedValues('visibility', [
            null,
            \ReflectionMethod::IS_PUBLIC,
            \ReflectionMethod::IS_PROTECTED,
            \ReflectionMethod::IS_PRIVATE,
        ]);
        $resolver->setDefault('regex', null);
        $resolver->setAllowedTypes('regex', ['null', 'string']);
        $options = $resolver->resolve($options);

        $methods = [];
        while ($rClass) {
            foreach ($rClass->getMethods(filter: $options['visibility']) as $rMethod) {
                if ($options['regex'] && !@preg_match($options['regex'], $rMethod->getName(), $match)) {
                    continue;
                }
                if (!isset($methods[$rMethod->getName()])) {
                    $methods[$rMethod->getName()] = $rMethod;
                }
            }
            $rClass = $rClass->getParentClass();
        }

        ksort($methods);

        return $methods;
    }

    private static function createReflectionException(\ReflectionMethod $rMethod, string $message): ReflectionException
    {
        return new ReflectionException(sprintf(
            '%s::%s() %s.',
            $rMethod->getDeclaringClass()->getName(),
            $rMethod->getName(),
            $message,
        ));
    }
}
