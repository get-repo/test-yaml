<?php

namespace GetRepo\TestYaml\DependencyInjection;

use GetRepo\TestYaml\TestYaml;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class Container
{
    /** @var ContainerBuilder */
    private ContainerInterface $container;

    public function __construct(OutputInterface $output = new NullOutput())
    {
        $builder = \Closure::fromCallable(function (ContainerBuilder $container) use ($output) {
            $container->set('container', $this);
            $container->setDefinition(OutputInterface::class, new Definition(get_class($output)));
            $loader = new YamlFileLoader($container, new FileLocator(realpath(__DIR__ . '/../../config')));
            $loader->load('services.yml');
        });

        if (class_exists('\App\Kernel')) {
            $kernel = new class ($builder) extends \App\Kernel {
                public function __construct(
                    private \Closure $builder,
                ) {
                    parent::__construct('test', true); // @phpstan-ignore-line
                }

                protected function build(ContainerBuilder $container): void
                {
                    parent::build($container); // @phpstan-ignore-line
                    ($this->builder)($container);
                }

                public function getProjectDir(): string
                {
                    return $GLOBALS['__test_yaml_root'];
                }
            };
            $kernel->boot(); // @phpstan-ignore-line
            $this->container = $kernel->getContainer(); // @phpstan-ignore-line
        } else {
            $this->container = new ContainerBuilder();
            $builder($this->container);
            $this->container->compile();
        }
    }

    public function getTestYaml(): TestYaml
    {
        return $this->container->get(TestYaml::class);
    }
}
