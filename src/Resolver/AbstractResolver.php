<?php

namespace GetRepo\TestYaml\Resolver;

class AbstractResolver
{
    public const NAMESPACE_VARIABLE = 'variable';
    public const NAMESPACE_MOCK = 'mock';
    public const NAMESPACE_INSTANCE = 'instance';

    protected function plural(string $singular): string
    {
        // so far we just want to add append with an "s"
        return sprintf('%ss', $singular);
    }
}
