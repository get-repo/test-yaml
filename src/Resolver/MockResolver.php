<?php

namespace GetRepo\TestYaml\Resolver;

use Mockery;
use Mockery\MockInterface;

class MockResolver extends AbstractResolver
{
    public function __invoke(array $definitions): MockInterface
    {
        $options = $definitions['options'];
        /** @var MockInterface $mock */
        $mock = Mockery::mock($definitions['class']);

        if ($options['partial']) {
            $mock->makePartial();
        }

        if ($definitions['properties']) {
            $rClass = (new \ReflectionClass($mock));
            foreach ($definitions['properties'] as $property => $value) {
                if ($rClass->hasProperty($property)) {
                    $rClass->getProperty($property)->setValue($mock, $value);
                } else {
                    $mock->{$property} = $value;
                }
            }
        }

        foreach ($definitions['methods'] as $method => $expectations) {
            if (!$expectations) { // make simple mock
                $mock->shouldReceive($method);
                continue;
            }
            foreach ($expectations as $expectation) {
                $returnMethod = sprintf('andReturn%s', ($expectation['return_self'] ?? false) ? 'Self' : '');
                $returnValue = array_key_exists('return', $expectation) ? $expectation['return'] : null;
                if ($expectation['params']) {
                    $mock->shouldReceive($method) // @phpstan-ignore-line
                        ->with(...array_values($expectation['params']))
                        ->{$returnMethod}($returnValue);
                } else {
                    $mock->shouldReceive($method)->{$returnMethod}($returnValue);
                }
            }
        }

        return $mock;
    }
}
