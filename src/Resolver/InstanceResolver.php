<?php

namespace GetRepo\TestYaml\Resolver;

use GetRepo\TestYaml\Util\ClassUtil;

class InstanceResolver extends AbstractResolver
{
    public function __invoke(array $definitions): object
    {
        return ClassUtil::newInstance($definitions['class'], $definitions['constructor']);
    }
}
