<?php

namespace GetRepo\TestYaml\Resolver;

use GetRepo\TestYaml\DependencyInjection\ParameterBag;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompileResolver extends AbstractResolver
{
    private const DEFAULT_NAMESPACE_MAPPING = [
        self::NAMESPACE_VARIABLE => false, // false = won't check for resolver
        self::NAMESPACE_MOCK => true,      // true = check for resolver
        self::NAMESPACE_INSTANCE => true,  // true = check for resolver
    ];
    private const UNSET_NAMESPACES = [self::NAMESPACE_VARIABLE, self::NAMESPACE_MOCK, self::NAMESPACE_INSTANCE];

    public function __construct(
        private MockResolver $mockResolver,
        private InstanceResolver $instanceResolver,
    ) {
    }

    public function __invoke(array $data, array $options = []): array
    {
        $optionsResolver = new OptionsResolver();
        $optionsResolver->setDefault('namespaces', []);
        $optionsResolver->setAllowedTypes('namespaces', 'bool[]');
        $optionsResolver->setDefault('unsets', []);
        $optionsResolver->setAllowedTypes('unsets', 'string[]');
        $options = $optionsResolver->resolve($options);
        $namespaces = array_merge(self::DEFAULT_NAMESPACE_MAPPING, $options['namespaces']);

        $bag = new ParameterBag();
        foreach ($namespaces as $namespace => $resolve) {
            if (!$resolve) {
                $plural = $this->plural($namespace);
                foreach ($data[$plural] ?? [] as $name => $value) {
                    $bag->set(sprintf('%s.%s', $namespace, $name), $value);
                }
            }
        }

        foreach (array_keys(array_filter($namespaces)) as $namespace) {
            $plural = $this->plural($namespace);
            foreach ($data[$plural] ?? [] as $name => $value) {
                $this->resolveNamespace($data, $bag, $namespace, $name, $value);
            }
        }

        $data = $bag->resolveValue($data);

        // unset unnecessary namespaces
        foreach (array_merge(self::UNSET_NAMESPACES, $options['unsets']) as $namespace) {
            $namespace = $this->plural($namespace);
            unset($data[$namespace]);
        }

        return $data;
    }

    private function resolveNamespace(
        array $data,
        ParameterBag $bag,
        string $namespace,
        string $name,
        mixed $value,
        array $resolving = [],
    ): void {
        $walkRecursiveCallback = function (mixed $val) use ($data, $bag, $resolving): void {
            $namespaces = implode('|', array_keys(array_filter(self::DEFAULT_NAMESPACE_MAPPING)));
            if (
                is_string($val)
                && preg_match('/^%(' . $namespaces . ')\.([^%\s]+)%$/', $val, $match)
                && !$bag->has(trim($val, '%'))
                && !in_array($val, $resolving)
            ) {
                $plural = $this->plural($match[1]);
                if (isset($data[$plural][$match[2]])) {
                    $resolving[] = $val;
                    $this->resolveNamespace(
                        $data,
                        $bag,
                        $match[1],
                        $match[2],
                        $data[$plural][$match[2]],
                        $resolving,
                    );
                }
            }
        };

        if (!$resolving && $all = $bag->all()) {
            array_walk_recursive($all, $walkRecursiveCallback);
            unset($all);
        }

        if (is_array($value)) {
            array_walk_recursive($value, $walkRecursiveCallback);
        }

        $propertyName = sprintf('%sResolver', $namespace);
        if (!property_exists($this, $propertyName)) {
            throw new \RuntimeException(sprintf(
                'Property %s::$%s was not found for namespace "%s".',
                self::class,
                $propertyName,
                $namespace,
            ));
        }
        if (!$this->{$propertyName} instanceof AbstractResolver) {
            throw new \RuntimeException(sprintf(
                'Property %s::$%s is not an object instance of %s.',
                self::class,
                $propertyName,
                AbstractResolver::class,
            ));
        }

        $bag->set(sprintf('%s.%s', $namespace, $name), ($this->{$propertyName})($bag->resolveValue($value)));
    }
}
