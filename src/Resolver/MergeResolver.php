<?php

namespace GetRepo\TestYaml\Resolver;

use Symfony\Component\OptionsResolver\OptionsResolver;

class MergeResolver extends AbstractResolver
{
    private const DEFAULT_NAMESPACES_MAP = [
        self::NAMESPACE_VARIABLE => true, // true means plural
        self::NAMESPACE_MOCK => true,     // true means plural
        self::NAMESPACE_INSTANCE => true, // true means plural
    ];

    public function __invoke(array $a, array $b, array $options = []): array
    {
        $optionsResolver = new OptionsResolver();
        $optionsResolver->setRequired('namespaces');
        $optionsResolver->setAllowedTypes('namespaces', 'bool[]');
        $options = $optionsResolver->resolve($options);

        foreach (array_merge(self::DEFAULT_NAMESPACES_MAP, $options['namespaces']) as $namespace => $plural) {
            if ($plural) {
                $namespace = $this->plural($namespace);
            }
            if (!array_key_exists($namespace, $a)) {
                continue;
            }
            if (!array_key_exists($namespace, $b)) {
                $b[$namespace] = $a[$namespace];
                continue;
            }
            switch (sprintf('%s-%s', $aType = gettype($a[$namespace]), $bType = gettype($b[$namespace]))) {
                case 'array-array':
                    $b[$namespace] = array_merge($a[$namespace], $b[$namespace]);
                    break;
                default:
                    throw new \RuntimeException(sprintf(
                        'Unable to merge namespace "%s" with types %s and %s',
                        $namespace,
                        $aType,
                        $bType,
                    ));
            }
        }

        return $b;
    }
}
