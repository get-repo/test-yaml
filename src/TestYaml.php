<?php

namespace GetRepo\TestYaml;

use GetRepo\TestYaml\Configuration\AbstractConfiguration;
use GetRepo\TestYaml\Exception\TestFailedException;
use GetRepo\TestYaml\Loader\TestFactory;
use GetRepo\TestYaml\Loader\TestFileFinder;
use GetRepo\TestYaml\Output\ConsoleOutput;
use GetRepo\TestYaml\Output\ErrorOutput;
use Symfony\Component\Config\Definition\Exception\Exception as ConfigException;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\String\ByteString;

class TestYaml
{
    /** @var AbstractConfiguration[] */
    private array $configurations = [];
    /** @var TestFactory[] */
    private array $testFactories = [];

    public function __construct(
        private TestFileFinder $testFileFinder,
        private ConsoleOutput $output,
        private ErrorOutput $errorOutput,
        /** @var AbstractConfiguration[] $configurations */
        #[TaggedIterator('getrepo_testyaml.configuration')]
        iterable $configurations,
        /** @var TestFactory[] $testFactories */
        #[TaggedIterator('getrepo_testyaml.test_factory')]
        iterable $testFactories,
    ) {
        $this->configurations = $this->iteratorToArray($configurations, 'Configuration');
        $this->testFactories = $this->iteratorToArray($testFactories, 'TestFactory');
    }

    public function __invoke(array $paths, string $filter = null): bool
    {
        $status = true;
        foreach (array_keys($this->testFactories) as $name) {
            $this->output->writeln(sprintf('%s TESTS', strtoupper($name)));

            // check configuration exists
            if (!isset($this->configurations[$name])) {
                throw $this->createMissingClassException('configuration', $name);
            }
            // check configuration exists
            if (!isset($this->testFactories[$name])) {
                throw $this->createMissingClassException('test factory', $name);
            }

            $configuration = $this->configurations[$name];
            $factory = $this->testFactories[$name];

            // find related test files
            $files = ($this->testFileFinder)(
                paths: $paths,
                extension: $factory->getFileExtension(),
            );

            if (!$files) {
                $this->output->warn("\xE2\x9A\xA0 No files found", true);
                $this->output->writeln('');
                continue;
            }

            try {
                $tests = ($factory)($configuration, $files);
                if ($filter) {
                    foreach ($tests as $i => &$test) {
                        if (!str_contains($test->getFullName(), $filter)) {
                            $test = null;
                        }
                    }
                    unset($test);
                    $tests = array_filter($tests);
                }
            } catch (ConfigException $e) {
                $this->output->err(sprintf("YAML error: %s", $e->getMessage()), true);
                $this->output->writeln('');
                continue;
            }

            if (!$tests) {
                $this->output->warn("\xE2\x9A\xA0 No tests found", true);
                $this->output->writeln('');
                continue;
            }

            $failures = [];
            foreach ($tests as $i => $test) {
                if (!$test->isEnabled()) {
                    $this->output->write('<fg=gray;options=bold>D</>');
                    continue;
                }

                try {
                    $test->run();
                    $this->output->write('<fg=green>.</>');
                } catch (TestFailedException $e) {
                    $status = false;
                    $failures[] = $test;
                    $this->output->write('<fg=red;options=bold>F</>');
                }
                if ((ConsoleOutput::SIZE - 1) === $i % ConsoleOutput::SIZE) {
                    $this->output->writeln('');
                }
            }
            $this->output->writeln('');

            if ($failures) {
                $this->errorOutput->print($failures);
            }
            $this->output->writeln('');
        }

        return $status;
    }

    private function iteratorToArray(iterable $objects, string $prefix): array
    {
        $array = [];
        foreach ($objects as $object) {
            $matches = (new ByteString(get_class($object)))->match(sprintf('/\\\(\w+)%s$/', $prefix));
            if (!isset($matches[1])) {
                throw new \RuntimeException(sprintf('Invalid "%s" prefix to convert iterable to array', $prefix));
            }
            $array[(string)(new ByteString($matches[1]))->lower()] = $object;
        }

        return $array;
    }

    private function createMissingClassException(string $type, string $name): \RuntimeException
    {
        return throw new \RuntimeException(sprintf(
            'The %1$s class for test type "%2$s" was not found.' . PHP_EOL .
            'Maybe you forgot to create the related %1$s class ?',
            $type,
            $name,
        ));
    }
}
