<?php

namespace GetRepo\TestYaml\Loader;

use Symfony\Component\Finder\Finder;

class TestFileFinder
{
    public function __invoke(array $paths, string $extension): array
    {
        $files = [];
        foreach ($paths as $k => $path) {
            if (is_file($path)) {
                if (str_ends_with($path, $extension) && $path = realpath($path)) {
                    $files[] = $path;
                }
                unset($paths[$k]);
            }
        }

        if ($paths) {
            $finder = (new Finder())->files()
                ->in($paths)
                ->name(sprintf('*.%s', $extension))
                ->followLinks();

            foreach ($finder as $file) {
                $files[] = $file->getRealPath();
            }
        }

        return $files;
    }
}
