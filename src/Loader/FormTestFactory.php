<?php

namespace GetRepo\TestYaml\Loader;

use GetRepo\TestYaml\Loader\Test\TestInterface;
use Symfony\Component\DependencyInjection\Attribute\AsTaggedItem;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Contracts\Service\Attribute\Required;

#[AsTaggedItem(priority: 3)]
class FormTestFactory extends TestFactory
{
    private ?FormFactoryInterface $formFactory = null;

    #[Required]
    public function setFormFactory(
        FormFactoryInterface $formFactory = null,
    ): void {
        $this->formFactory = $formFactory;
    }

    protected function mergeNamespaces(): array
    {
        return [
            'data' => false,    // false means no plural
            'options' => false, // false means no plural
        ];
    }

    /** @return \GetRepo\TestYaml\Loader\Test\UnitTest */
    protected function build(
        string $testClassName,
        string $testName,
        string $itemTestName,
        string $assertName,
        array $init,
        array $test,
        array $asserts,
    ): TestInterface {
        return new $testClassName(
            testName: $testName,
            itemTestName: $itemTestName,
            assertName: $assertName,
            className: $init['class'],
            test: $test,
            asserts: $asserts,
            formFactory: $this->formFactory,
        );
    }
}
