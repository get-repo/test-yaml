<?php

namespace GetRepo\TestYaml\Loader\Test;

use Exception;
use GetRepo\TestYaml\Exception\TestFailedException;

abstract class AbstractTest implements TestInterface
{
    private ?Exception $exception = null;

    abstract protected function doRun(): void;

    public function __construct(
        protected string $testName,
        protected string $itemTestName,
        protected string $assertName,
        protected array $test,
        protected array $asserts,
    ) {
    }

    public function isEnabled(): bool
    {
        return $this->test['enabled'];
    }

    public function getFile(): string
    {
        return $this->test['file'];
    }

    public function getTestName(): string
    {
        return $this->testName;
    }

    public function getItemTestName(): string
    {
        if ($this->itemTestName === (string)intval($this->itemTestName)) {
            return sprintf('#%d', intval($this->itemTestName) + 1);
        }

        return $this->itemTestName;
    }

    public function getAssertName(): string
    {
        if ($this->assertName === (string)intval($this->assertName)) {
            return sprintf('#%d', intval($this->assertName) + 1);
        }

        return $this->assertName;
    }

    public function getFullName(): string
    {
        return sprintf(
            '%s %s %s %s',
            basename($this->getFile()),
            $this->getTestName(),
            $this->getItemTestName(),
            $this->getAssertName(),
        );
    }

    public function hasFailed(): bool
    {
        return $this->exception instanceof Exception;
    }

    public function getException(): ?Exception
    {
        return $this->exception;
    }

    public function run(): void
    {
        try {
            $this->doRun();
        } catch (\Exception $e) {
            throw new TestFailedException(origin: $this->exception = $e);
        }
    }
}
