<?php

namespace GetRepo\TestYaml\Loader\Test;

use GetRepo\TestYaml\Util\AssertUtil;
use GetRepo\TestYaml\Util\ClassUtil;
use Symfony\Component\PropertyAccess\PropertyAccess;

class EntityTest extends AbstractTest
{
    public function __construct(
        protected string $className,
        string $testName,
        string $itemTestName,
        string $assertName,
        array $test,
        array $asserts,
    ) {
        parent::__construct(
            testName: $testName,
            itemTestName: $itemTestName,
            assertName: $assertName,
            test: $test,
            asserts: $asserts,
        );
    }

    protected function doRun(): void
    {
        $rClass = new \ReflectionClass($this->className);
        $rMethod = $rClass->getMethod($this->testName);

        if ($rMethod->isConstructor()) {
            throw new \ReflectionException('Unable to test constructor.');
        }
        if ($rMethod->isStatic()) {
            throw new \ReflectionException('Unable to test static method.');
        }
        if (!$rMethod->isPublic()) {
            throw new \ReflectionException('Method must be public.');
        }

        $data = $this->test['data'];

        // prepare constructor parameters
        $constructorParameterNames = [];
        if ($rConstructor = $rClass->getConstructor()) {
            foreach ($rConstructor->getParameters() as $rParameter) {
                $constructorParameterNames[] = strtolower($rParameter->getName());
            }
        }
        $constructorParameters = [];
        foreach ($data as $name => $value) {
            $nameLower = strtolower($name);
            if (in_array($nameLower, $constructorParameterNames)) {
                $constructorParameters[$name] = $value;
                unset($data[$name]);
            }
        }

        ClassUtil::validateMethodParameters($rConstructor, $constructorParameters);
        ClassUtil::validateMethodParameters($rMethod, $this->test['parameters']);
        $instance = ClassUtil::newInstance($this->className, $constructorParameters);
        // setters
        if ($data) {
            $pa = PropertyAccess::createPropertyAccessorBuilder()
                ->enableExceptionOnInvalidIndex()
                ->enableExceptionOnInvalidPropertyPath()
                ->getPropertyAccessor();
            foreach ($data as $name => $value) {
                $pa->setValue($instance, $name, $value);
            }
        }
        AssertUtil::runAndCatchException(
            \Closure::fromCallable([$instance, $rMethod->name]),
            $this->test['parameters'],
            $this->asserts,
        );
    }
}
