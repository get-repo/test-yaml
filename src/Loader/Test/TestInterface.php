<?php

namespace GetRepo\TestYaml\Loader\Test;

use Exception;

interface TestInterface
{
    public function isEnabled(): bool;
    public function getFile(): string;
    public function getTestName(): string;
    public function getItemTestName(): string;
    public function getAssertName(): string;
    public function getFullName(): string;
    public function hasFailed(): bool;
    public function getException(): ?Exception;
    public function run(): void;
}
