<?php

namespace GetRepo\TestYaml\Loader\Test;

use GetRepo\TestYaml\Util\AssertUtil;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Validator\Validation;

class FormTest extends AbstractTest
{
    public function __construct(
        protected string $className,
        string $testName,
        string $itemTestName,
        string $assertName,
        array $test,
        array $asserts,
        private ?FormFactoryInterface $formFactory = null,
    ) {
        parent::__construct(
            testName: $testName,
            itemTestName: $itemTestName,
            assertName: $assertName,
            test: $test,
            asserts: $asserts,
        );
    }

    protected function doRun(): void
    {
        if (!$this->formFactory) {
            $this->formFactory = Forms::createFormFactoryBuilder()
                ->addExtension(new ValidatorExtension(Validation::createValidator()))
                ->getFormFactory();
        }
        $data = $this->test['data'] ?? null;
        $form = $this->formFactory->create($this->className, $data, $this->test['options']);

        $form->submit($data);
        $data = $form->getData();
        $actual = [
            'data' => $data,
            'form' => $form,
            'config' => $form->getConfig(),
            'valid' => $form->isValid(),
            'errors' => $this->getErrorMessages($form),
            'view' => $form->createView()->children,
        ];
        AssertUtil::run($actual, $this->asserts);
    }

    private function getErrorMessages(FormInterface $form): array
    {
        $errors = [];

        /** @var \Symfony\Component\Form\FormError $error */
        foreach ($form->getErrors() as $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isSubmitted() || !$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
