<?php

namespace GetRepo\TestYaml\Loader\Test;

use GetRepo\TestYaml\Util\AssertUtil;
use GetRepo\TestYaml\Util\ClassUtil;

class UnitTest extends AbstractTest
{
    public function __construct(
        protected string $className,
        string $testName,
        string $itemTestName,
        string $assertName,
        array $test,
        array $asserts,
    ) {
        parent::__construct(
            testName: $testName,
            itemTestName: $itemTestName,
            assertName: $assertName,
            test: $test,
            asserts: $asserts,
        );
    }

    protected function doRun(): void
    {
        $rMethod = new \ReflectionMethod($this->className, $this->testName);
        $rClass = $rMethod->getDeclaringClass();

        if (!$rMethod->isPublic()) {
            throw new \ReflectionException('Method must be public.');
        }

        // create class instance based on method type
        if ($rMethod->isConstructor()) {
            $closure = \Closure::fromCallable([$this->className, $rMethod->name]);
            $parameters = $this->test['constructor'];
        } else {
            if ($rMethod->isStatic()) {
                $closure = \Closure::fromCallable([$rClass->name, $rMethod->name]);
            } else {
                ClassUtil::validateMethodParameters($rMethod, $this->test['parameters']);
                $instance = ClassUtil::newInstance($this->className, $this->test['constructor']);
                $closure = \Closure::fromCallable([$instance, $rMethod->name]);
            }

            $parameters = $this->test['parameters'];
        }

        AssertUtil::runAndCatchException($closure, $parameters, $this->asserts);
    }
}
