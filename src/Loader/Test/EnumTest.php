<?php

namespace GetRepo\TestYaml\Loader\Test;

use GetRepo\TestYaml\Util\AssertUtil;
use GetRepo\TestYaml\Util\ClassUtil;

class EnumTest extends AbstractTest
{
    public function __construct(
        protected string $className,
        string $testName,
        string $itemTestName,
        string $assertName,
        array $test,
        array $asserts,
    ) {
        parent::__construct(
            testName: $testName,
            itemTestName: $itemTestName,
            assertName: $assertName,
            test: $test,
            asserts: $asserts,
        );
    }

    protected function doRun(): void
    {
        $rEnum = new \ReflectionEnum($this->className);

        // method test
        if ($rEnum->hasMethod($this->testName)) {
            $rMethod = $rEnum->getMethod($this->testName);
            if (!$rMethod->isPublic()) {
                throw new \ReflectionException(sprintf('Enum method %s() is not public', $this->testName));
            }
            $caseName = $this->test['case'];
            if (!$rMethod->isStatic() && !$caseName) {
                throw new \ReflectionException(sprintf('Non-static method %s() needs a case value', $this->testName));
            }
            ClassUtil::validateMethodParameters($rMethod, $this->test['parameters']);
            $closure = \Closure::fromCallable([
                $caseName ? $rEnum->getCase($caseName)->getValue() : $this->className,
                $rMethod->name,
            ]);
        } elseif ($rEnum->hasCase($this->testName)) {
            if ($this->test['parameters']) {
                throw new \LogicException('Enum cases does not use "parameters"');
            }
            $closure = \Closure::fromCallable([$rEnum->getCase($this->testName), 'getValue']);
        } else {
            throw new \ReflectionException(sprintf('Invalid enum test name "%s"', $this->testName));
        }

        AssertUtil::runAndCatchException($closure, $this->test['parameters'], $this->asserts);
    }
}
