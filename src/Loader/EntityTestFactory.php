<?php

namespace GetRepo\TestYaml\Loader;

use GetRepo\TestYaml\Loader\Test\TestInterface;
use Symfony\Component\DependencyInjection\Attribute\AsTaggedItem;

#[AsTaggedItem(priority: 4)]
class EntityTestFactory extends TestFactory
{
    protected function mergeNamespaces(): array
    {
        return [
            'data' => false, // false means no plural
        ];
    }

    /** @return \GetRepo\TestYaml\Loader\Test\UnitTest */
    protected function build(
        string $testClassName,
        string $testName,
        string $itemTestName,
        string $assertName,
        array $init,
        array $test,
        array $asserts,
    ): TestInterface {
        return new $testClassName(
            testName: $testName,
            itemTestName: $itemTestName,
            assertName: $assertName,
            className: $init['class'],
            test: $test,
            asserts: $asserts,
        );
    }
}
