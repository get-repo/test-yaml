<?php

namespace GetRepo\TestYaml\Loader;

use GetRepo\TestYaml\Configuration\AbstractConfiguration;

interface TestFactoryInterface
{
    public function __invoke(AbstractConfiguration $configuration, array $tests): array;
    public function getFileExtension(): string;
}
