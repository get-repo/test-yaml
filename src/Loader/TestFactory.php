<?php

namespace GetRepo\TestYaml\Loader;

use GetRepo\TestYaml\Configuration\AbstractConfiguration;
use GetRepo\TestYaml\Loader\Test\AbstractTest;
use GetRepo\TestYaml\Loader\Test\TestInterface;
use GetRepo\TestYaml\Resolver\CompileResolver;
use GetRepo\TestYaml\Resolver\MergeResolver;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\String\ByteString;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Symfony\Component\Yaml\Yaml;

abstract class TestFactory implements TestFactoryInterface
{
    abstract protected function mergeNamespaces(): array;
    abstract protected function build(
        string $testClassName,
        string $testName,
        string $itemTestName,
        string $assertName,
        array $init,
        array $test,
        array $asserts,
    ): TestInterface;

    public function __construct(
        private CompileResolver $compileResolver,
        private MergeResolver $mergeResolver,
    ) {
    }

    public function getFileExtension(): string
    {
        return (new ByteString(static::class))
            ->replaceMatches('/^.*\\\(\w+)TestFactory/', '$1')
            ->lower()
            ->append('.test.yml');
    }

    /** @return AbstractTest[] */
    public function __invoke(AbstractConfiguration $configuration, array $testFiles): array
    {
        $builds = [];
        foreach ($testFiles as $testFile) {
            $yaml = Yaml::parseFile($testFile, Yaml::PARSE_DATETIME | Yaml::PARSE_CONSTANT | Yaml::PARSE_CUSTOM_TAGS);
            array_walk_recursive($yaml, function (&$v) {
                if ($v instanceof TaggedValue) {
                    $v = match ($v->getTag()) {
                        'datetime' => new \DateTime($v->getValue()),
                        'datetime_immutable' => new \DateTimeImmutable($v->getValue()),
                        default => $v->getValue(),
                    };
                }
            });
            $yaml = (new Processor())->processConfiguration($configuration, [$yaml]);
            $tests = $yaml['tests'];
            unset($yaml['tests']);
            foreach ($tests as $testName => $itemTests) {
                foreach ($itemTests as $itemTestName => $itemTest) {
                    $asserts = $itemTest['asserts'];
                    unset($itemTest['asserts']);
                    $itemTest['file'] = $testFile;
                    foreach ($asserts as $assertName => $assert) {
                        $builds[] = $this->prepareBuild(
                            $testName,
                            $itemTestName,
                            $assertName,
                            $yaml,
                            $itemTest,
                            $assert,
                        );
                    }
                }
            }
        }

        /** @var AbstractTest[] $builds */
        return $builds;
    }

    private function prepareBuild(
        string $testName,
        string $itemTestName,
        string $assertName,
        array $initGlobal,
        array $initTest,
        array $assert,
    ): TestInterface {
        // cleaning assert from empty array
        foreach (['array_keys'] as $k) {
            if ([] === ($assert[$k] ?? null)) {
                unset($assert[$k]);
            }
        }

        // merge global and test data
        $initTest = ($this->mergeResolver)(
            $initGlobal['init'],
            $initTest,
            ['namespaces' => $this->mergeNamespaces()],
        );

        // merge enable global and test data
        if (!$initGlobal['enabled']) {
            $initTest['enabled'] = false;
        }

        // compile data (with asset)
        $initTest['assert'] = $assert;
        $initTest = ($this->compileResolver)($initTest);
        $asserts = $initTest['assert'];
        unset($assert, $initTest['assert']); // remove assert to be separate

        return $this->build(
            $this->getTestClassName(),
            $testName,
            $itemTestName,
            $assertName,
            $initGlobal,
            $initTest,
            $asserts,
        );
    }

    private function getTestClassName(): string
    {
        $factoryClass = get_class($this);
        $testClass = (string)(new ByteString($factoryClass))
            ->replaceMatches('/\\\(\w+)TestFactory$/', '\\\Test\\\$1Test');
        if (!class_exists($testClass)) {
            throw new \RuntimeException(sprintf(
                'The test class %s (for the test factory %s) was not found.',
                $testClass,
                $factoryClass,
            ));
        }

        return $testClass;
    }
}
