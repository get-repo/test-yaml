<?php

namespace GetRepo\TestYaml\Exception;

use Exception;
use Throwable;

class TestFailedException extends Exception
{
    protected ?Throwable $origin;

    public function __construct(string $message = null, Throwable $origin = null)
    {
        $this->origin = $origin;
        if (!$message && $origin) {
            $message = $origin->getMessage();
        }
        parent::__construct(message: $message, previous: $origin);
    }

    public function getOrigin(): ?Throwable
    {
        return $this->origin;
    }
}
