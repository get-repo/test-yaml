<?php

namespace GetRepo\TestYaml\Configuration;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class EnumConfiguration extends AbstractConfiguration
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('enum');
        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->canBeDisabled()
            ->children()
                ->scalarNode('class')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('init')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->append($this->getVariablesNode())
                        ->append($this->getMocksNode())
                    ->end()
                ->end()
                ->arrayNode('tests')
                    ->arrayPrototype() // method name
                        ->arrayPrototype() // test
                            ->canBeDisabled()
                            ->children()
                                ->scalarNode('case')
                                    ->defaultNull()
                                    ->cannotBeEmpty()
                                ->end()
                                ->arrayNode('parameters')
                                    ->variablePrototype()->end()
                                ->end()
                                ->append($this->getVariablesNode())
                                ->append($this->getMocksNode())
                                ->append($this->getAssertNode())
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
