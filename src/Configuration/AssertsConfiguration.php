<?php

namespace GetRepo\TestYaml\Configuration;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class AssertsConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('asserts');
        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->arrayPrototype()
                ->children()
                    ->enumNode('type')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->values([
                            'variable',
                            'scalar',
                            'boolean',
                            'integer',
                            'array[scalar]',
                            'array[boolean]',
                            'array[integer]',
                        ])
                    ->end()
                    ->scalarNode('method')
                        ->defaultNull()
                    ->end()
                    ->booleanNode('negate')
                        ->defaultFalse()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
