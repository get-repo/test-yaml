<?php

namespace GetRepo\TestYaml\Configuration;

use GetRepo\TestYaml\Util\AssertUtil;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\ConfigurationInterface;

abstract class AbstractConfiguration implements ConfigurationInterface
{
    protected function getAssertNode(): ArrayNodeDefinition
    {
        $node = new ArrayNodeDefinition('asserts');

        /** @var \Symfony\Component\Config\Definition\Builder\NodeBuilder $builder */
        $builder = $node
            ->requiresAtLeastOneElement()
            ->arrayPrototype()
                ->validate()
                    ->ifTrue(function (array $asserts) {
                        return 0 === count($asserts);
                    })
                    ->thenInvalid('Requires at least one assert.')
                ->end()
                ->children();
        foreach (AssertUtil::getConfig() as $name => $assert) {
            $type = $assert['type'];
            $arrayType = null;
            if (preg_match('/^(array)\[([a-z]+)\]$/', $type, $match)) {
                $type = $match[1];
                $arrayType = $match[2];
            }
            if ($assert['property_path']) {
                /** @var \Symfony\Component\Config\Definition\Builder\NodeBuilder $node */
                $node = $builder->arrayNode($name)->children();
                $node->scalarNode('path')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end();
                if ($arrayType) {
                    $node->arrayNode('value')->prototype($arrayType)->end();
                } else {
                    $node->node('value', $type)
                        ->isRequired()
                    ->end();
                }
                $node->end();
            } else {
                if ($arrayType) {
                    $builder->arrayNode($name)->prototype($arrayType)->end();
                } else {
                    $builder->node($name, $type)->end();
                }
            }
        }

        return $builder->end()->end(); // @phpstan-ignore-line
    }

    protected function getMocksNode(): ArrayNodeDefinition
    {
        $node = new ArrayNodeDefinition('mocks');

        return $node
            ->arrayPrototype()
                ->children()
                    ->scalarNode('class')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->arrayNode('properties')
                        ->variablePrototype()->end()
                    ->end()
                    ->arrayNode('methods')
                        ->arrayPrototype()
                            ->arrayPrototype()
                                ->addDefaultsIfNotSet()
                                ->beforeNormalization()
                                    ->ifTrue(function ($v): bool {
                                        return !(array_key_exists('return', $v)
                                            xor array_key_exists('return_self', $v));
                                    })
                                    ->thenInvalid('Either "return" & "return_self" keys, not both in %s')
                                ->end()
                                ->validate()
                                    ->ifTrue(function ($v): bool {
                                        return array_key_exists('return_self', $v);
                                    })
                                    ->then(function ($v) {
                                        $v['return_self'] = true;

                                        return $v;
                                    })
                                ->end()
                                ->children()
                                    ->arrayNode('params')
                                        ->variablePrototype()->end()
                                    ->end()
                                    ->variableNode('return')
                                    ->end()
                                    ->booleanNode('return_self')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('options')
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->booleanNode('partial')
                                ->defaultFalse()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    protected function getInstancesNode(): ArrayNodeDefinition
    {
        $node = new ArrayNodeDefinition('instances');

        return $node
            ->arrayPrototype()
                ->children()
                    ->scalarNode('class')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->arrayNode('constructor')
                        ->variablePrototype()->end()
                    ->end()
                ->end()
            ->end();
    }

    protected function getVariablesNode(): ArrayNodeDefinition
    {
        return (new ArrayNodeDefinition('variables'))->variablePrototype()->end(); // @phpstan-ignore-line
    }

    protected function getDataNode(): ArrayNodeDefinition
    {
        return (new ArrayNodeDefinition('data'))->variablePrototype()->end(); // @phpstan-ignore-line
    }
}
