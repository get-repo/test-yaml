<?php

namespace GetRepo\TestYaml\Configuration;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class UnitConfiguration extends AbstractConfiguration
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('unit');
        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->canBeDisabled()
            ->children()
                ->scalarNode('class')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('init')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('constructor')
                            ->variablePrototype()->end()
                        ->end()
                        ->append($this->getVariablesNode())
                        ->append($this->getMocksNode())
                        ->append($this->getInstancesNode())
                    ->end()
                ->end()
                ->arrayNode('tests')
                    ->arrayPrototype() // method name
                        ->arrayPrototype() // test
                            ->canBeDisabled()
                            ->children()
                                ->arrayNode('constructor')
                                    ->variablePrototype()->end()
                                ->end()
                                ->arrayNode('parameters')
                                    ->variablePrototype()->end()
                                ->end()
                                ->append($this->getVariablesNode())
                                ->append($this->getMocksNode())
                                ->append($this->getInstancesNode())
                                ->append($this->getAssertNode())
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
