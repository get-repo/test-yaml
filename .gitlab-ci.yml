stages:
  - &stage_php_syntax PHP syntax
  - &stage_coding_standards coding standards
  - &stage_unit unit tests

# syntax
syntax-php82: &syntax
  image: &image registry.gitlab.com/symfonyaml/core-bundle:latest
  stage: *stage_php_syntax
  before_script:
    - export PHP_VERSION=8.2
  script:
    - update-alternatives --set php /usr/bin/php${PHP_VERSION}
    - composer global require liuggio/fastest
    - find . -name "*.php" | /root/.config/composer/vendor/bin/fastest -v "php -l {} > /dev/null;"

syntax-php83:
  <<: *syntax
  before_script:
    - export PHP_VERSION=8.3

# phpcs
phpcs-php82: &phpcs
  image: *image
  stage: *stage_coding_standards
  before_script:
    - export PHP_VERSION=8.2
  script:
    - update-alternatives --set php /usr/bin/php${PHP_VERSION}
    - composer update --no-progress --no-interaction --ansi --no-scripts
    - vendor/bin/phpcs

phpcs-php83:
  <<: *phpcs
  before_script:
    - export PHP_VERSION=8.3

# phpmd
phpmd-php82: &phpmd
  image: *image
  stage: *stage_coding_standards
  cache: &cache
    key: test-yaml-${CI_JOB_NAME}
    paths:
      - .composer-cache
  before_script:
    - export PHP_VERSION=8.2
  script:
    - update-alternatives --set php /usr/bin/php${PHP_VERSION}
    - composer update --no-progress --no-interaction --ansi --no-scripts
    - vendor/bin/phpmd ./src ansi phpmd.xml
    - vendor/bin/phpmd ./tests ansi phpmd.xml

phpmd-php83:
  <<: *phpmd
  before_script:
    - export PHP_VERSION=8.3

# php-cs-fixer
phpcsfixer-php82: &phpcsfixer
  image: *image
  stage: *stage_coding_standards
  cache: *cache
  before_script:
    - export PHP_VERSION=8.2
  script:
    - update-alternatives --set php /usr/bin/php${PHP_VERSION}
    - composer update --no-progress --no-interaction --ansi --no-scripts
    - vendor/bin/php-cs-fixer fix --ansi --using-cache=no --dry-run --diff

phpcsfixer-php83:
  <<: *phpcsfixer
  before_script:
    - export PHP_VERSION=8.3

# phpstan
phpstan-php82: &phpstan
  image: *image
  stage: *stage_coding_standards
  cache: *cache
  before_script:
    - export PHP_VERSION=8.2
  script:
    - update-alternatives --set php /usr/bin/php${PHP_VERSION}
    - composer update --no-progress --no-interaction --ansi --no-scripts
    - vendor/bin/phpstan analyse

phpstan-php83:
  <<: *phpstan
  before_script:
    - export PHP_VERSION=8.3

# unit
unit-php82: &unit
  image: *image
  stage: *stage_unit
  cache: *cache
  before_script:
    - export PHP_VERSION=8.2
  script:
    - update-alternatives --set php /usr/bin/php${PHP_VERSION}
    - composer update --no-progress --no-interaction --ansi --no-scripts
    - vendor/bin/phpunit tests/

unit-php83:
  <<: *unit
  before_script:
    - export PHP_VERSION=8.3

# runner
runner-php82: &runner
  image: *image
  stage: *stage_unit
  cache: *cache
  before_script:
    - export PHP_VERSION=8.2
  script:
    - update-alternatives --set php /usr/bin/php${PHP_VERSION}
    - composer update --no-progress --no-interaction --ansi --no-scripts
    - php bin/runner

runner-php83:
  <<: *runner
  before_script:
    - export PHP_VERSION=8.3
